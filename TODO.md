# ssh support

- https://git.libssh.org/projects/libssh.git/
- https://pcre2project.github.io/pcre2/doc/html/pcre2build.html

# tcp / udp / shell 'expect' support

- https://github.com/rwmjones/miniexpect
