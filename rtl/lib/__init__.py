ZIGLANG_TARGETS = [
    "aarch64-linux-musl",
    "arm-linux-musleabi",
    "i386-linux-musl",
    "mips-linux-musl",
    "powerpc64le-linux-musl",
    "powerpc64-linux-musl",
    "powerpc-linux-musl",
    "x86_64-linux-musl",
]

RTL_DEFAULT_TARGET = "x86_64-linux-musl"
