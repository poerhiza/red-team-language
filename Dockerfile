FROM alpine:3.18 as rtl

ENV PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/ziglang/"

WORKDIR /

ARG ZIG_VERSION=zig-linux-x86_64-0.11.0-dev.4059+17255bed4

RUN mkdir -p /opt/rtl /src

RUN wget https://ziglang.org/builds/${ZIG_VERSION}.tar.xz && \
    tar -xvJf /${ZIG_VERSION}.tar.xz && \
    mv /${ZIG_VERSION} /ziglang/ && rm -f /${ZIG_VERSION}.tar.xz;

RUN apk add --no-cache \
    alpine-sdk \
    clang \
    python3 \
    py3-pip \
    llvm14-dev \
    llvm14 \
    linux-headers \
    xz

RUN  mkdir -p /opt/rtl /src && \
    ln -s /usr/bin/llvm14-config /usr/bin/llvm-config \
    && \
    pip3 install -U pip && \
    pip3 install red-team-language

WORKDIR /src

ENTRYPOINT ["/usr/bin/rtl"]

